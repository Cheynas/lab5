package sim;

/**
 * The main queue of events that will happen in the course of the program.
 * 
 * @author Cheynas
 * 
 */

public class EventQueue {
	private Event[] queue = new Event[0];

	/**
	 * To add an event in the queue.
	 * 
	 * @param event
	 *            The event to place in the queue.
	 */

	public void add(Event event) {
		Event[] temp = new Event[queue.length + 1];
		System.arraycopy(queue, 0, temp, 0, queue.length);
		temp[queue.length] = event;
		this.queue = temp;
		sort();
	}

	private void sort() {
		Event temp = null;
		for (int i = 0; i < queue.length; i += 1) {
			for (int j = i + 1; j < queue.length; j += 1) {
				if (queue[j].getTime() < queue[i].getTime()) {
					temp = queue[i];
					queue[i] = queue[j];
					queue[j] = temp;
					j = i+1;
				}
			}
		}
	}

	/**
	 * Removes the first event in the queue, if there is any left.
	 */

	public void removeNext() {
		if (this.hasNext()) {
			if (queue.length > 1) {
				Event[] temp = new Event[this.queue.length - 1];
				System.arraycopy(this.queue, 1, temp, 0, queue.length - 1);
				this.queue = temp;
			} else {
				this.queue = new Event[0];
			}
		}
	}

	/**
	 * Checks if the queue has a next event
	 * 
	 * @return True if there is an event in the queue.
	 */

	public boolean hasNext() {
		if (queue.length > 0) {
			return true;
		}
		return false;
	}

	/**
	 * Getter for the next event to occur.
	 * 
	 * @return The first event in the queue.
	 */

	public Event getNext() {
		return queue[0];
	}

	public String toString() {
		String s = "Type: EventQueue,";
		for (int i = 0; i < queue.length; i += 1) {
			s += " [" + i + "] = " + queue[i].toString();
		}
		return s;
	}

}
