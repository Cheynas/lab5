package sim;


/**
 * The end event for the simulation.
 * 
 * @author Cheynas
 *
 */

public class SimEnd extends Event {
	Sim sim;

	/**
	 * The constructor for the end event.
	 * @param time The time the simulation should end.
	 */
	
	public SimEnd(double time, SimState state, Sim sim) {
		super(time, state);
		this.sim = sim;
	}

	/**
	 * Ends the simulation.
	 */
	
	public void execute() {
		state.update(time, this);
		sim.running = false;
	}
	
	public String toString() {
		return "Stop";
	}
}
