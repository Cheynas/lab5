package sim;

/**
 * The start event for the simulation.
 * 
 * @author Cheynas
 * 
 */

public class SimStart extends Event {
	private Sim sim;
	
	/**
	 * The constructor for the start event.
	 * @param sim The simulator this event should run on.
	 */

	public SimStart(SimState state, Sim sim) {
		super(0.0, state);
		this.sim = sim;
	}

	/**
	 * Starts the simulator.
	 */

	public void execute() {
		state.update(time, this);
		sim.running = true;
		state.start();
	}
	
	public String toString() {
		return "Start";
	}
}
