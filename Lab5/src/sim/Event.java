package sim;

import carWash.state.Car;

/**
 * The event class that contains the blueprint of every event that needs to be made.
 * 
 * @author Cheynas
 *
 */

public abstract class Event{
	protected double time;
	protected SimState state;
	protected Car car = null;

	/**
	 * Constructor for every event.
	 * @param time The time this event should happen.
	 * @param state The state this event should refer to.
	 */
	public Event(double time, SimState state) {
		this.time = time;
		this.state = state;
	}
	
	/**
	 * Getter for the time.
	 * @return The time of this events happenings.
	 */
	public double getTime() {
		return time;
	}
	
	public Car getCar() {
		return car;
	}

	public abstract void execute();
	
	public String toString() {
		return "Type: Event";
	}

}
