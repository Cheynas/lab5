package sim;

import java.util.Observable;

/**
 * An place-holder for the states that it will handle.
 * 
 * It's mostly for the sim-class to handle any kind of states from here.
 * 
 * @author Cheynas
 *
 */

public abstract class SimState extends Observable {
	protected EventQueue eq;
	
	public SimState(EventQueue eq){
		this.eq = eq;
	}
	
	public EventQueue getEventQueue(){
		return this.eq;
	}
	
	public abstract void update(double time, Event event);

	public abstract void start();
	
	public String toString() {
		return "Type: State";
	}
}