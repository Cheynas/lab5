package sim;

import java.util.Observable;
import java.util.Observer;

/**
 * An abstract class for the simulation view.
 * 
 * Mostly so it's not forgotten when making own simulations.
 * 
 * @author Cheynas
 *
 */

public abstract class SimView implements Observer{
	protected SimState state;
	
	/**
	 * An constructor for the viewer.
	 * @param state The state that the view should observe.
	 */
	
	public SimView(SimState state) {
		this.state = state;
	}

	public abstract void update(Observable o, Object arg);
	
	public String toString() {
		return "Type: View";
	}
}
