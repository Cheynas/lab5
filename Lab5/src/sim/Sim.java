package sim;



/**
 * The main simulation that loops through the events and executes them in order.
 * 
 * @author Cheynas
 * 
 */

public class Sim {
	private EventQueue queue;
	public boolean running = false;

	/**
	 * The constructor for the main simulation.
	 * @param state The state the simulation should get the event queue from.
	 */
	
	public Sim(SimState state) {
		this.queue = state.getEventQueue();
	}
	
	/**
	 * The start event for the simulation.
	 * @param start The start event that should initialize the simulation.
	 */
	
	public void run(Event start){
		start.execute();
		while (queue.hasNext() && running){
			queue.getNext().execute();
			queue.removeNext();
		}
	}

	public String toString() {
		return "Type: Sim";
	}
}
