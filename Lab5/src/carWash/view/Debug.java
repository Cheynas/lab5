package carWash.view;

import java.util.Observable;

import sim.SimState;
import sim.SimView;

public class Debug extends SimView {
	private static final String format = "%1$-5S %2$-100S | %3$-100S \r\n";
	private static boolean debug = false;

	public Debug(SimState state) {
		super(state);
	}

	public void update(Observable o, Object arg) {
		if (!debug) {
			startDebug();
		}
		System.out.format(format, "", o.getClass(), o);
	}

	public static void write(Object arg) {
		if (debug) {
			System.out.format(format, "->", arg.getClass(), arg);
		}
	}

	public static void startDebug() {
		System.out.format(format, "", "CLASS", "TOSTRING()");
		debug = true;
	}
}