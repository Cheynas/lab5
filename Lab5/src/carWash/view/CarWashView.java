package carWash.view;

import java.util.Observable;

import carWash.state.*;

import sim.*;

/**
 * A console printing of the happenings in the queue.
 * 
 * @author Cheynas
 * 
 */

public class CarWashView extends SimView {
	private final String format = "%1$-10.2f %2$-10d %3$-10d %4$-10s %5$-10s %6$-20.2f %7$-20.2f %8$-20d %9$d \r\n";

	public CarWashView(CarWashState state) {
		super(state);
		System.out.println("Fast machines: " + state.getFreeWashers()[0]);
		System.out.println("Slow machines: " + state.getFreeWashers()[1]);
		System.out.println("Fast distribution: (" + state.fastDistrobution[0] + ", " + state.fastDistrobution[1] + ")");
		System.out.println("Slow distribution: (" + state.slowDistrobution[0] + ", " + state.slowDistrobution[1] + ")");
		System.out.println("Exponential distribution with lambda = "+ state.lamda);
		System.out.println("Seed = " + state.seed);
		System.out.println("Max Queue size: " + state.maxQueue);
		System.out.println("-------------------------------------------");
		System.out.format("%1$-10s %2$-10s %3$-10s %4$-10s %5$-10s %6$-20s %7$-20s %8$-20s %9$s \r\n",
						"Time", "Fast", "Slow", "Id", "Event", "IdleTime", "QueueTime", "QueueSize", "Rejected");
	}

	/**
	 * The update class of an observer.
	 */

	public void update(Observable o, Object arg) {
		CarWashState state = (CarWashState) o;
		Event event = (Event) arg;
		double time = event.getTime();
		int fast = state.getFreeWashers()[0];
		int slow = state.getFreeWashers()[1];
		Object id;
		double idle = state.getIdleTime();
		double queue = state.getQueueTime();
		int size = state.getQueueSize();
		int rejected = state.getRejectedCars();

		if (event.getCar() == null) {
			id = "-";
		} else {
			id = event.getCar().GetID();
		}

		System.out.format(format, time, fast, slow, id, event, idle, queue, size, rejected);
		if (event.getClass() == sim.SimEnd.class) {
			System.out.println("-------------------------------------------");
			System.out.format("%1$-30s %2$5.2f \r\n", "Total idle machine time: ", state.getIdleTime());
			System.out.format("%1$-30s %2$5.2f \r\n", "Total queueing time: ",state.getQueueTime());
			System.out.format("%1$-30s %2$5.2f \r\n", "Mean queueing time: ", (state.getQueueTime() / state.getServicedCars()));
			System.out.format("%1$-30s %2$5d \r\n", "Rejected cars: ", state.getRejectedCars());
		}
	}
}
