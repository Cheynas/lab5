package carWash;

import carWash.state.CarWashState;
import carWash.view.*;
import sim.*;

/**
 * The main class that starts the simulation
 * 
 * @author Cheynas
 *
 */

public class CarWashMain {
	
	/**
	 * Creates the states and sends them to the simulation.
	 */

	public static void main(String[] args) {
		CarWashState state = new CarWashState();
		Sim sim = new Sim(state);

		state.addObserver(new CarWashView(state));
		state.getEventQueue().add(new SimEnd(15.0, state, sim));
		sim.run(new SimStart(state, sim));
	}
}
