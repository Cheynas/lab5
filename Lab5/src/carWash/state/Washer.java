package carWash.state;

/**
 * A car-wash class that keeps track of it's condition
 * 
 * @author Cheynas
 * 
 */

public class Washer {
	private RandomStreams speed;
	private boolean isFree = true;
	private Car car = null;

	/**
	 * Constructor for the washer.
	 * 
	 * @param fastSlow The rand-object that will give the time for completion.
	 */
	public Washer(RandomStreams speed) {
		this.speed = speed;
	}

	/**
	 * Makes the car-washer available for use.
	 */

	public void makeAvailable() {
		this.isFree = true;
		this.car = null;
	}

	/**
	 * Makes the car-washer occupied.
	 * @param car The car to occupy the washer.
	 */

	public void makeOccupied(Car car) {
		this.isFree = false;
		this.car = car;
	}

	/**
	 * Checks if the car-wash is available.
	 * 
	 * @return True if it is available.
	 */

	public boolean checkAvailable() {
		return isFree;
	}

	/**
	 * Gets the time it will take until it is finished.
	 * 
	 * @return The time the washer should take.
	 */

	public double getFinished() {
		return speed.next();
	}
	
	/**
	 * Gets the car the washer is washing.
	 * 
	 * @return The car in the washer
	 */
	
	public Car getCar(){
		return car;
	}

	public String toString() {
		return "Type: Washer";
	}
}