package carWash.state;

import java.util.NoSuchElementException;

/**
 * My own written FIFO function
 * 
 * @author Cheynas
 *
 */

public class FIFO {

	Object[] list = new Object[0];
	int maxSize = 0;

	/**
	 * Adds the object to the end of the FIFO queue.
	 * @param arg0 The object to add.
	 */
	
	public void add(Object arg0) {
		Object[] temp = new Object[list.length + 1];
		System.arraycopy(list, 0, temp, 0, list.length);
		temp[temp.length - 1] = arg0;
		list = temp;
		if (list.length < maxSize) {
			maxSize = list.length;
		}
	}
	
	/**
	 * Gets the first object in the queue.
	 * 
	 * @return The first object
	 * @throws NoSuchElementException If the queue is empty.
	 */
	
	public Object first() throws NoSuchElementException {
		if (this.isEmpty()) {
			throw new NoSuchElementException("Queue is empty");
		}
		return list[0];
	}

	/**
	 * Checks if the queue is empty.
	 * @return True if it is empty.
	 */
	
	public boolean isEmpty() {
		if (list.length == 0) {
			return true;
		}
		return false;
	}
	
	/**
	 * Gets the highest size the queue has ever had.
	 * @return The highest size.
	 */

	public int maxSize() {
		return maxSize;
	}
	
	/**
	 * Removes the first element of the queue.
	 * @throws NoSuchElementException If the queue is empty.
	 */

	public void removeFirst() throws NoSuchElementException {
		if (this.isEmpty()) {
			throw new NoSuchElementException("Queue is empty");
		}
		Object[] temp = new Object[list.length - 1];
		System.arraycopy(list, 1, temp, 0, temp.length);
		list = temp;
	}

	/**
	 * Gives the current size of the list.
	 * @return The current size of the list.
	 */
	
	public int size() {
		return list.length;
	}
	
	public boolean equals(Object arg0) {
		if (arg0.getClass() != FIFO.class) {
			return false;
		}
		FIFO f = (FIFO) arg0;
		if (this.size() == f.size()) {
			for (int i = 0; i < this.size(); i += 1) {
				if (this.list[i] == null || f.list[i] == null) {
					if (this.list[i] == null && f.list[i] == null) {
						continue;
					} else {
						return false;
					}
				}
				if (!(this.list[i].equals(f.list[i]))) {
					return false;
				}
			}
			return true;
		}
		return false;
	}

	public String toString() {
		String text = "Queue: ";
		for (Object obj : list) {
			text += "(" + String.valueOf(obj) + ") ";
		}
		return text;
	}
}
