package carWash.state;

/**
 * A car for being pushed around.
 * 
 * @author Cheynas
 *
 */

public class Car {
	private int ID;
	private Washer washer;

	/**
	 * The constructor for the car.
	 * @param ID The ID number the car should have-
	 */
	
	public Car(int ID) {
		this.ID = ID;
	}
	
	/**
	 * Gets the ID number of the car.
	 * @return The ID number.
	 */

	public int GetID() {
		return ID;
	}

	/**
	 * Sets the washer for this car
	 * @param washer The washer to wash this car.
	 */
	public void setWasher(Washer washer) {
		this.washer = washer;
	}

	/**
	 * Get the washer for this car
	 * @return The washer that washers this car.
	 */
	public Washer getWasher() {
		return washer;
	}
	
	
	public String toString() {
		return "Type: Car, ID #" + ID;
	}
}
