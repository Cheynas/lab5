package carWash.state;

import random.ExponentialRandomStream;
import carWash.events.CarArrive;

/**
 * A class that makes car objects that will be washed.
 * 
 * @author Cheynas
 *
 */

public class CarFactory {
	private int nextCarID = 0;
	private CarWashState state;
	private RandomStreams random;
	private double time = 0.0;
	
	/**
	 * Constructor for a CarFactory.
	 * @param s The CarWashState that the CarFactory should report to.
	 */
	
	public CarFactory(CarWashState s){
		this.state = s;
		this.random = new ExponentialRandomStream(s.lamda, s.seed);
	}
	
	/**
	 * Creates a new car.
	 */
	
	public void createNewCar(){
		Car newCar = new Car(nextCarID++);
		time += random.next();
		state.getEventQueue().add(new CarArrive(time, state, newCar));
	}
	
	public String toString(){
		return "Type: CarFactory";
	}
}