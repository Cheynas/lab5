package carWash.state;

/**
 * An abstract class that the random functions inherit from.
 * 
 * @author Cheynas
 *
 */

public abstract class RandomStreams {
	
	public abstract double next();
	
	public String toString() {
		return "Type: RandomStream";
	}
}
