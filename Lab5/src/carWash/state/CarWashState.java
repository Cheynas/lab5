package carWash.state;

import random.UniformRandomStream;
import sim.*;
import carWash.events.CarLeave;

/**
 * The top state of the car-wash program.
 * 
 * Contains most of the information for the program
 * 
 * @author Cheynas
 * 
 */

public class CarWashState extends SimState {
	
	// The preset values go here.	
	private final int fast = 2;
	private final int slow = 4;
	public final double[] fastDistrobution = { 2.8, 5.6 };
	public final double[] slowDistrobution = { 4.5, 6.7 };
	public final double lamda = 1.5;
	public final int seed = 1234;
	public final int maxQueue = 7;
	
	// The private variable functions go here.
	private Washer[][] washers = new Washer[2][];
	private FIFO queue;
	private double currentTime = 0.0;
	private double idleTime = 0.0;
	private double queueTime = 0.0;
	private int availableFastWashers;
	private int availableSlowWashers;
	private int rejectedCars;
	private int servicedCars;
	private CarFactory factory;

	private RandomStreams fastWash;
	private RandomStreams slowWash;

	/**
	 * The constructor of a CarWash program.
	 * 
	 * @param maxQ
	 *            The highest size the waiting queue should have.
	 * @param fast
	 *            The number of fast washing machines
	 * @param slow
	 *            The number of slow washing machines
	 */

	public CarWashState() {
		super(new EventQueue());
		this.queue = new FIFO();
		this.currentTime = 0.0;
		this.availableFastWashers = fast;
		this.availableSlowWashers = slow;
		this.rejectedCars = 0;
		this.servicedCars = 0;
		this.factory = new CarFactory(this);

		this.fastWash = new UniformRandomStream(fastDistrobution[0], fastDistrobution[1], seed);
		this.slowWash = new UniformRandomStream(slowDistrobution[0], slowDistrobution[1], seed);

		this.washers[0] = new Washer[fast];
		for (int i = 0; i < fast; i += 1) {
			this.washers[0][i] = new Washer(fastWash);
		}
		
		this.washers[1] = new Washer[slow];
		for (int i = 0; i < slow; i += 1) {
			this.washers[1][i] = new Washer(slowWash);
		}
	}

	/**
	 * To add a car into the waiting queue.
	 * 
	 * @param car
	 *            The car to add.
	 */

	public void add(Car car) {
		if (queue.size() < this.maxQueue) {
			queue.add(car);
			servicedCars += 1;
		} else {
			this.rejectedCars += 1; // Queue is full
		}
	}
	
	/**
	 * Gets the first car from the queue.
	 * 
	 * @return the first car in the queue.
	 */

	public Car getFirst() {
		return (Car) queue.first();
	}

	/**
	 * Removes a car from the queue to wash.
	 */

	public void removeFirst() {
		queue.removeFirst();
	}

	/**
	 * Checks if the queue is empty.
	 * 
	 * @return True if the queue is empty.
	 */

	public boolean isEmpty() {
		return queue.isEmpty();
	}

	/**
	 * Updates the queue times and the current time compared to the new time.
	 * @param time The new time to add the queuing to.
	 */
	
	public void update(double time, Event event) {
		double timeDiff = time - currentTime;
		for (Washer wash : washers[0]) {
			if (wash.checkAvailable()) {
				idleTime += timeDiff;
			}
		}
		for (Washer wash : washers[1]) {
			if (wash.checkAvailable()) {
				idleTime += timeDiff;
			}
		}
		for (int i = 0; i < queue.size(); i += 1) {
			queueTime += timeDiff;
		}
		this.currentTime = time;
		setChanged();
		notifyObservers(event);
	}
	
	/**
	 * Washes a car, if possible.
	 * 
	 * @param car The car to attempt to wash.
	 * @return True if the car is being washed.
	 */

	public boolean enterWasher(Car car) {
		for (Washer w : this.washers[0]) {
			if (w.checkAvailable()) {
				w.makeOccupied(car);
				eq.add(new CarLeave(currentTime + w.getFinished(), this, car));
				availableFastWashers -= 1;
				queue.removeFirst();
				return true;
			}
		}
		for (Washer w : this.washers[1]) {
			if (w.checkAvailable()) {
				w.makeOccupied(car);
				eq.add(new CarLeave(currentTime + w.getFinished(), this, car));
				availableSlowWashers -= 1;
				queue.removeFirst();
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Makes the car finish washing and make the washer available.
	 * 
	 * @param car The car to finish washing.
	 */
	
	public void leaveWasher(Car car) {
		for (Washer w : this.washers[0]) {
			if (w.getCar() == car) {
				w.makeAvailable();
				availableFastWashers += 1;
				return;
			}
		}
		for (Washer w : this.washers[1]) {
			if (w.getCar() == car) {
				w.makeAvailable();
				availableSlowWashers += 1;
				return;
			}
		}
	}

	// Only getters beyond this point.
	
	/**
	 * Gets the number of free washers.
	 * 
	 * @return An int[] with the numbers of free Fast washers at index 0 and
	 *         number of free Slow washers at index 1.
	 */

	public int[] getFreeWashers() {
		int[] washers = { availableFastWashers, availableSlowWashers };
		return washers;
	}

	/**
	 * Gets the current time for printing.
	 * 
	 * @return The current time for the state.
	 */
	public double getCurrentTime() {
		return currentTime;
	}

	/**
	 * Gets the current queue size.
	 * 
	 * @return The size the current queue has.
	 */
	public int getQueueSize() {
		return queue.size();
	}

	/**
	 * Gets the number of rejected cars.
	 * 
	 * @return The number of rejected cars.
	 */
	public int getRejectedCars() {
		return rejectedCars;
	}

	/**
	 * Gets the time the washers have been idle.
	 * 
	 * @return The time they have stood idle.
	 */
	public double getIdleTime() {
		return idleTime;
	}

	/**
	 * Gets the time cars have been waiting in the queue.
	 * 
	 * @return The time the cars have been waiting.
	 */
	public double getQueueTime() {
		return queueTime;
	}

	/**
	 * Gets the number of serviced cars.
	 * @return The number of serviced cars.
	 */
	public int getServicedCars() {
		return servicedCars;
	}

	/**
	 * Tells the factory to make a new car.
	 */

	public void newCar() {
		factory.createNewCar();
	}

	public void start() {
		newCar();
	}
}
