package carWash.events;

import sim.Event;
import sim.SimState;
import carWash.state.Car;
import carWash.state.CarWashState;

/**
 * Makes a car arrive at the washer.
 * 
 * @author Cheynas
 * 
 */

public class CarArrive extends Event {

	/**
	 * The constructor of the Event.
	 * 
	 * @param time The time the car should arrive at the washer.
	 * @param state The state that the event should look at.
	 * @param car The car that should arrive.
	 */

	public CarArrive(double time, SimState state, Car car) {
		super(time, state);
		this.car = car;
	}

	/**
	 * Makes the car arrive at the washer.
	 */

	public void execute() {
		CarWashState state = (CarWashState) this.state;
		state.update(time, this);

		state.add(car);
		state.getEventQueue().add(new CarWash(time,state));
		state.newCar();
	}

	public String toString() {
		return "Arrive";
	}
}