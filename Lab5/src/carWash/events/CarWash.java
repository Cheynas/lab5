/**
 * 
 */
package carWash.events;

import sim.Event;
import sim.SimState;
import carWash.state.CarWashState;

/**
 * @author cheynas
 *
 */
public class CarWash extends Event {

	/**
	 * Constructor for the event.
	 * @param time The time the car should be washed.
	 * @param state The state that the event should look at.
	 * @param car The car that should be washed.
	 */
	public CarWash(double time, SimState state) {
		super(time, state);
	}

	/** 
	 *  Washes the first car in the queue.
	 */
	public void execute() {
		CarWashState state = (CarWashState) this.state;
		if (state.getQueueSize() > 0){
			state.enterWasher(state.getFirst());
		}
	}

}
