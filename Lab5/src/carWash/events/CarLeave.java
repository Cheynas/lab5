package carWash.events;

import carWash.state.Car;
import carWash.state.CarWashState;
import sim.Event;
import sim.SimState;

/**
 * Makes the car leave the washer.
 * 
 * @author Cheynas
 *
 */

public class CarLeave extends Event {
	
	/**
	 * The constructor for the event.
	 * @param time The time the car should leave the washer.
	 * @param state The state that the event should look at.
	 * @param car The car that should be washed.
	 */

	public CarLeave(double time, SimState state, Car car) {
		super(time, state);
		this.car = car;
	}

	/**
	 * Makes the car leave the washer.
	 */
	
	public void execute() {
		CarWashState state = (CarWashState) this.state;
		state.update(time, this);
		
		state.leaveWasher(car);
		state.getEventQueue().add(new CarWash(time,state));
	}
	
	public Car getCar() {
		return this.car;
	}
	
	public String toString() {
		return "Leave";
	}
}